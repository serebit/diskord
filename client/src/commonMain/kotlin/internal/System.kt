package com.serebit.strife.internal

/** The name of the operating system. */
internal expect val osName: String

internal expect val Throwable.stackTraceAsString: String
