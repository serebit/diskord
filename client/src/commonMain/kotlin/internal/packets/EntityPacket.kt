package com.serebit.strife.internal.packets

/** A packet representation of an [com.serebit.strife.entities.Entity] */
internal interface EntityPacket {
    val id: Long
}
